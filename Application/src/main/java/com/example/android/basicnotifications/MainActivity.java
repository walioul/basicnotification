package com.example.android.basicnotifications;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.HttpURLConnection;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.ExecutionException;

/**
 * The entry point to the BasicNotification sample.
 */
public class MainActivity extends Activity {

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sample_layout);

        Button button= (Button) findViewById(R.id.basic_notification);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new BasicNotification().execute();
            }
        });

        Button large_image_notification= (Button) findViewById(R.id.large_image_notification);
        large_image_notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new LargeImageNotification().execute();
            }
        });




    }

    public  Map<String,String> parse(JSONObject json , Map<String,String> out) throws JSONException{
        Iterator<String> keys = json.keys();
        while(keys.hasNext()){
            String key = keys.next();
            String val = null;
            try{
                JSONObject value = json.getJSONObject(key);
                parse(value,out);
            }catch(Exception e){
                val = json.getString(key);
            }

            if(val != null){
                out.put(key,val);
            }
        }
        return out;
    }


    private class LargeImageNotification extends AsyncTask<Void,String, String>{

        private int notification_id=2;
        Bitmap image;
        String image_raw;
        String title;
        String content;
        String resource_url;

        HttpURLConnection conn;
        String json = "";

        @Override
        protected String doInBackground(Void... params) {
            try {
                URL urlObj = new URL("http://connectingmass.com/notification_test/notifications.php");
                conn = (HttpURLConnection) urlObj.openConnection();
                conn.setDoOutput(true);
                conn.setRequestMethod("POST");
                conn.setRequestProperty("Accept-Charset", "UTF-8");
                conn.setReadTimeout(15000);
                conn.setConnectTimeout(20000);
                conn.connect();


                //Receive the response from the server
                InputStream in = new BufferedInputStream(conn.getInputStream());
                BufferedReader reader = new BufferedReader(new InputStreamReader(in));
                StringBuilder result = new StringBuilder();
                String line;
                while ((line = reader.readLine()) != null) {
                    result.append(line);
                }
                json = result.toString();
                Log.d("All Notifications",  result.toString());

            } catch (SocketTimeoutException e) {

            }catch (IOException m){
                m.printStackTrace();
            }
            conn.disconnect();
            return json;
        }

        @Override
        protected void onPostExecute(String json) {


            try{
                JSONObject object = new JSONObject(json);

                JSONObject info = object.getJSONObject("2");

                Map<String,String> notification = new HashMap<String, String>();

                parse(info,notification);

                image_raw = notification.get("image");
                title = notification.get("notification_header");
                content = notification.get("notification_text");
                resource_url = notification.get("resource_url");
                image = new GetImage(image_raw).execute().get();


                Intent intent = new Intent(Intent.ACTION_VIEW,
                        Uri.parse(resource_url));
                PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), 0, intent, 0);

                NotificationCompat.Builder builder = new NotificationCompat.Builder(getApplicationContext());

                builder.setSmallIcon(R.drawable.ic_stat_notification);
                builder.setContentIntent(pendingIntent);
                builder.setAutoCancel(true);
                builder.setLargeIcon(image);
                builder.setStyle(new NotificationCompat.BigPictureStyle().bigPicture(image));
                builder.setContentTitle(title);
                builder.setContentText(content);

                NotificationManager notificationManager = (NotificationManager) getSystemService(
                        NOTIFICATION_SERVICE);
                notificationManager.notify(notification_id, builder.build());

            }catch (JSONException j){
                j.printStackTrace();
            }catch (InterruptedException ie){
                ie.printStackTrace();
            }catch (ExecutionException ee){
                ee.printStackTrace();
            }

        }



    }




    private class BasicNotification extends AsyncTask<Void,String, String>{

        /**
         * A numeric value that identifies the notification that we'll be sending.
         * This value needs to be unique within this app, but it doesn't need to be
         * unique system-wide.
         */
        private int notification_id=1;
        Bitmap image;
        String image_raw;
        String title;
        String content;
        String resource_url;

        HttpURLConnection conn;
        String json = "";

        @Override
        protected String doInBackground(Void... params) {
            try {
                URL urlObj = new URL("http://connectingmass.com/notification_test/notifications.php");
                conn = (HttpURLConnection) urlObj.openConnection();
                conn.setDoOutput(true);
                conn.setRequestMethod("POST");
                conn.setRequestProperty("Accept-Charset", "UTF-8");
                conn.setReadTimeout(15000);
                conn.setConnectTimeout(20000);
                conn.connect();


                //Receive the response from the server
                InputStream in = new BufferedInputStream(conn.getInputStream());
                BufferedReader reader = new BufferedReader(new InputStreamReader(in));
                StringBuilder result = new StringBuilder();
                String line;
                while ((line = reader.readLine()) != null) {
                    result.append(line);
                }
                json = result.toString();
                Log.d("All Notifications",  result.toString());

            } catch (SocketTimeoutException e) {

            }catch (IOException m){
                m.printStackTrace();
            }
            conn.disconnect();
            return json;
        }

        @Override
        protected void onPostExecute(String json) {


            try{
                JSONObject object = new JSONObject(json);

                JSONObject info = object.getJSONObject("1");

                Map<String,String> notification = new HashMap<String, String>();

                parse(info,notification);

                image_raw = notification.get("image");
                title = notification.get("notification_header");
                content = notification.get("notification_text");
                resource_url = notification.get("resource_url");
                image = new GetImage(image_raw).execute().get();



                // BEGIN_INCLUDE(build_action)
                /** Create an intent that will be fired when the user clicks the notification.
                 * The intent needs to be packaged into a {@link android.app.PendingIntent} so that the
                 * notification service can fire it on our behalf.
                 */
                Intent intent = new Intent(Intent.ACTION_VIEW,
                        Uri.parse(resource_url));
                PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), 0, intent, 0);
                // END_INCLUDE(build_action)

                // BEGIN_INCLUDE (build_notification)
                /**
                 * Use NotificationCompat.Builder to set up our notification.
                 */
                NotificationCompat.Builder builder = new NotificationCompat.Builder(getApplicationContext());

                /** Set the icon that will appear in the notification bar. This icon also appears
                 * in the lower right hand corner of the notification itself.
                 *
                 * Important note: although you can use any drawable as the small icon, Android
                 * design guidelines state that the icon should be simple and monochrome. Full-color
                 * bitmaps or busy images don't render well on smaller screens and can end up
                 * confusing the user.
                 */
                builder.setSmallIcon(R.drawable.ic_stat_notification);

                // Set the intent that will fire when the user taps the notification.
                builder.setContentIntent(pendingIntent);

                // Set the notification to auto-cancel. This means that the notification will disappear
                // after the user taps it, rather than remaining until it's explicitly dismissed.
                builder.setAutoCancel(true);


                /**
                 *Build the notification's appearance.
                 * Set the large icon, which appears on the left of the notification. In this
                 * sample we'll set the large icon to be the same as our app icon. The app icon is a
                 * reasonable default if you don't have anything more compelling to use as an icon.
                 */

                builder.setLargeIcon(image);

                /**
                 * Set the text of the notification. This sample sets the three most commononly used
                 * text areas:
                 * 1. The content title, which appears in large type at the top of the notification
                 * 2. The content text, which appears in smaller text below the title
                 * 3. The subtext, which appears under the text on newer devices. Devices running
                 *    versions of Android prior to 4.2 will ignore this field, so don't use it for
                 *    anything vital!
                 */
                builder.setContentTitle(title);
                builder.setContentText(content);
                //builder.setSubText("Tap to view documentation about notifications.");

                // END_INCLUDE (build_notification)

                // BEGIN_INCLUDE(send_notification)
                /**
                 * Send the notification. This will immediately display the notification icon in the
                 * notification bar.
                 */
                NotificationManager notificationManager = (NotificationManager) getSystemService(
                        NOTIFICATION_SERVICE);
                notificationManager.notify(notification_id, builder.build());

            }catch (JSONException j){
                j.printStackTrace();
            }catch (InterruptedException ie){
                ie.printStackTrace();
            }catch (ExecutionException ee){
                ee.printStackTrace();
            }

        }


    }

    private class GetImage extends AsyncTask<String, Void, Bitmap> {
        Bitmap nBitmapImage;
        HttpURLConnection conn;

        private String imageUrl;
        public GetImage(String url){
            this.imageUrl=url;
        }

        @Override
        protected Bitmap doInBackground(String... params) {
            InputStream in;

            try {
                URL urlObj = new URL(this.imageUrl);
                conn = (HttpURLConnection) urlObj.openConnection();
                conn.setDoOutput(true);
                conn.setRequestMethod("GET");
                //conn.setRequestProperty("Accept-Charset", "UTF-8");
                conn.setReadTimeout(15000);
                conn.setConnectTimeout(15000);
                conn.connect();
                in = conn.getInputStream();
                nBitmapImage = BitmapFactory.decodeStream(in);

            } catch (MalformedURLException e) {
                e.printStackTrace();
            }catch (IOException i){
                i.printStackTrace();
            }
            conn.disconnect();
            return nBitmapImage;
        }
    }

}


